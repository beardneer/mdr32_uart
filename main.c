 #include <MDR32F9Qx_port.h>
 #include <MDR32F9Qx_rst_clk.h>
 #include <MDR32F9Qx_power.h>
 #include <MDR32F9Qx_uart.h>
 
/* Private typedef -----------------------------------------------------------*/


/* Private define ------------------------------------------------------------*/


/* Private variables ---------------------------------------------------------*/
 UART_InitTypeDef UARTInitStruct;

 #define pause 1000000 //pause in microseconds
 #define fcpu 79 //load to timer

//_____________________________________________________________________________________________________

 void CLK_init(void);
 void SYSTICK_init(void);
 void GPIO_init(void);
 void UART_init(void);


 uint8_t Data = 32;
 uint8_t L_Data = 32;
 unsigned long long sys_time_count = pause;

 //__________________________________________________________
 
 int main()
 {
	 CLK_init();
	 SYSTICK_init();
	 GPIO_init();
	 UART_init();
	 
   while (1)
   {
		 if (Data != L_Data)
		 {
			 UART_SendData(MDR_UART2, Data);
			 L_Data = Data;
		 }
		 
		 if(sys_time_count == 0)
		 {
			 /*
			 do something
			 */
			 sys_time_count = pause;
		 }
   }
 }


 //Setup clock
 void CLK_init(void)
 {
	 RST_CLK_DeInit(); //����� �������� ������������
	 RST_CLK_HSEconfig(RST_CLK_HSE_ON); //��������� ������������ �� �������� ����������
	 //���� ���������� ������ �������� ����������
	 while(RST_CLK_HSEstatus() != SUCCESS)
	 {
		 __NOP();
	 }
	 RST_CLK_CPU_PLLcmd(ENABLE);
	 //CPU_C1_SEL
	 RST_CLK_CPU_PLLconfig(RST_CLK_CPU_PLLsrcHSEdiv1, RST_CLK_CPU_PLLmul10); //80 ���
	 //wait
	 while(RST_CLK_CPU_PLLstatus() != SUCCESS)
	 {
		 __NOP();
	 }

	 //CPU_C2_SEL
   //RST_CLK_CPU_PLLuse(DISABLE);
	 RST_CLK_CPU_PLLuse(ENABLE);
	 
	 //�������� ������������ �������� ������� (���� ����������)
	 //CPU_C3_SEL
   RST_CLK_CPUclkPrescaler(RST_CLK_CPUclkDIV1); 
	 //RST_CLK_CPUclkSelection(RST_CLK_CPUclkCPU_C3);
	 // ��������� ���������� ���������� ���������� SelectRI � LOW � ����������� BKP
   // �������� ������� 80 ���
   RST_CLK_PCLKcmd(RST_CLK_PCLK_BKP, ENABLE);
	 //MDR_BKP -> REG_0E |= DUcc_Mask & POWER_DUcc_upto_80MHz;
   POWER_DUccMode(POWER_DUcc_upto_80MHz);
   //������� �������� ������������
	 //HCLK_SEL
   RST_CLK_CPUclkSelection(RST_CLK_CPUclkCPU_C3);
	 //RST_CLK_HSIcmd(DISABLE); //��������� ���������� ���������
	 //��������� ������������
	 SystemCoreClockUpdate();
 } 
  
 //system timer setup
 void SYSTICK_init(void)
 {
	 SysTick->LOAD = fcpu;
	 SysTick->CTRL |= SysTick_CTRL_CLKSOURCE_Msk;
	 SysTick->CTRL |= SysTick_CTRL_TICKINT_Msk;
	 SysTick->CTRL |= SysTick_CTRL_ENABLE_Msk;
	 NVIC_EnableIRQ(SysTick_IRQn);
 }
 
 //GPIO setup
 void GPIO_init(void)
 {
	 PORT_InitTypeDef GPIOInitStruct;
   RST_CLK_PCLKcmd (RST_CLK_PCLK_PORTC, ENABLE); //clock port C
   PORT_StructInit(&GPIOInitStruct);
	 GPIOInitStruct.PORT_Pin = PORT_Pin_0; //VD3
   GPIOInitStruct.PORT_OE = PORT_OE_OUT;
   GPIOInitStruct.PORT_SPEED = PORT_SPEED_FAST;
   GPIOInitStruct.PORT_MODE = PORT_MODE_DIGITAL;
   PORT_Init(MDR_PORTC, &GPIOInitStruct);
	 
	 PORT_StructInit(&GPIOInitStruct);
	 GPIOInitStruct.PORT_Pin = PORT_Pin_1; //VD4
   GPIOInitStruct.PORT_OE = PORT_OE_OUT;
   GPIOInitStruct.PORT_SPEED = PORT_SPEED_FAST;
   GPIOInitStruct.PORT_MODE = PORT_MODE_DIGITAL;
   PORT_Init(MDR_PORTC, &GPIOInitStruct);
	 
	 PORT_StructInit(&GPIOInitStruct);
	 GPIOInitStruct.PORT_Pin = PORT_Pin_2; //SELECT
   GPIOInitStruct.PORT_OE = PORT_OE_IN;
   GPIOInitStruct.PORT_SPEED = PORT_SPEED_FAST;
   GPIOInitStruct.PORT_MODE = PORT_MODE_DIGITAL;
   PORT_Init(MDR_PORTC, &GPIOInitStruct);
	 
	 RST_CLK_PCLKcmd (RST_CLK_PCLK_PORTE, ENABLE); //clock port E 
	 PORT_StructInit(&GPIOInitStruct);
	 GPIOInitStruct.PORT_Pin = PORT_Pin_1; //DOWN
   GPIOInitStruct.PORT_OE = PORT_OE_IN;
   GPIOInitStruct.PORT_SPEED = PORT_SPEED_FAST;
   GPIOInitStruct.PORT_MODE = PORT_MODE_DIGITAL;
   PORT_Init(MDR_PORTE, &GPIOInitStruct);
	 
	 PORT_StructInit(&GPIOInitStruct);
	 GPIOInitStruct.PORT_Pin = PORT_Pin_3; //LEFT
   GPIOInitStruct.PORT_OE = PORT_OE_IN;
   GPIOInitStruct.PORT_SPEED = PORT_SPEED_FAST;
   GPIOInitStruct.PORT_MODE = PORT_MODE_DIGITAL;
   PORT_Init(MDR_PORTE, &GPIOInitStruct);
	 
	 RST_CLK_PCLKcmd (RST_CLK_PCLK_PORTB, ENABLE); //clock port B
	 PORT_StructInit(&GPIOInitStruct);
	 GPIOInitStruct.PORT_Pin = PORT_Pin_6; //RIGHT
   GPIOInitStruct.PORT_OE = PORT_OE_IN;
   GPIOInitStruct.PORT_SPEED = PORT_SPEED_FAST;
   GPIOInitStruct.PORT_MODE = PORT_MODE_DIGITAL;
   PORT_Init(MDR_PORTB, &GPIOInitStruct);
	 
	 PORT_StructInit(&GPIOInitStruct);
	 GPIOInitStruct.PORT_Pin = PORT_Pin_5; //UP
   GPIOInitStruct.PORT_OE = PORT_OE_IN;
   GPIOInitStruct.PORT_SPEED = PORT_SPEED_FAST;
   GPIOInitStruct.PORT_MODE = PORT_MODE_DIGITAL;
   PORT_Init(MDR_PORTB, &GPIOInitStruct);
 }
 
 //UART setup
 void UART_init(void)
 {
	 //RST_CLK_PCLKcmd(RST_CLK_PCLK_PORTF, ENABLE);  //���������� ������������ ����� F
	 PORT_InitTypeDef GPIOInitStruct;
	 PORT_StructInit(&GPIOInitStruct);
	 RST_CLK_PCLKcmd(RST_CLK_PCLK_PORTF, ENABLE);  //���������� ������������ ����� F
	 // ���������� ����� ���������� GPIOInitStruct ��� ����������� ������ UART
   GPIOInitStruct.PORT_PULL_UP = PORT_PULL_UP_OFF;
   GPIOInitStruct.PORT_PULL_DOWN = PORT_PULL_DOWN_OFF;
   GPIOInitStruct.PORT_PD_SHM = PORT_PD_SHM_OFF;
   GPIOInitStruct.PORT_PD = PORT_PD_DRIVER;
   GPIOInitStruct.PORT_GFEN = PORT_GFEN_OFF;
   GPIOInitStruct.PORT_FUNC = PORT_FUNC_OVERRID; 
	 GPIOInitStruct.PORT_SPEED = PORT_SPEED_MAXFAST;
	 GPIOInitStruct.PORT_MODE = PORT_MODE_DIGITAL;
	 // ������������ 1 ����� ����� PORTF ��� ������ (UART2_TX) 
   GPIOInitStruct.PORT_OE = PORT_OE_OUT;
   GPIOInitStruct.PORT_Pin = PORT_Pin_1;
   PORT_Init(MDR_PORTF, &GPIOInitStruct);
	 // ������������ 0 ����� ����� PORTF ��� ����� (UART2_RX)
   GPIOInitStruct.PORT_OE = PORT_OE_IN;
   GPIOInitStruct.PORT_Pin = PORT_Pin_0;
   PORT_Init(MDR_PORTF, &GPIOInitStruct);
	 //���������� ������������ UART2
   RST_CLK_PCLKcmd(RST_CLK_PCLK_UART2, ENABLE);
   // ������������� �������� �������� ������� ��� UART2
   UART_BRGInit(MDR_UART2, UART_HCLKdiv1);
	 //UART_InitTypeDef UARTInitStruct;
	 //UART_InitTypeDef UART_InitStructure;
	 UART_StructInit(&UARTInitStruct);
	 UART_DeInit(MDR_UART2);
	 // ���������� ����� ��� ���������� UART_InitStructure 
   UARTInitStruct.UART_BaudRate   = 9600;    //�������� ������� �������� ������
   UARTInitStruct.UART_WordLength = UART_WordLength8b; //����� �������� 8 ���
   UARTInitStruct.UART_StopBits   = UART_StopBits1;    //1 ���� ���
   UARTInitStruct.UART_Parity     = UART_Parity_No;    // ��� �������� ��������
   UARTInitStruct.UART_FIFOMode   = UART_FIFO_OFF;     // ���������� FIFO ������
   /* ���������� �������� �� ��������� � ������� */
   UARTInitStruct.UART_HardwareFlowControl = UART_HardwareFlowControl_RXE | UART_HardwareFlowControl_TXE;
	 UART_Init(MDR_UART2, &UARTInitStruct);
	 UART_ITConfig (MDR_UART2, UART_IT_RX, ENABLE);        //���������� ���������� �� ������ 
   UART_ITConfig (MDR_UART2, UART_IT_TX, ENABLE);        //���������� ���������� �� �������� ��������
	 UART_Cmd(MDR_UART2, ENABLE);
	 NVIC_EnableIRQ(UART2_IRQn);
 }

 //-------------------------------Interrupts----------------------------- 
 void UART2_IRQHandler()
 {
	 if (UART_GetFlagStatus(MDR_UART2, UART_FLAG_RXFF) == SET)
	 {
		 Data = UART_ReceiveData(MDR_UART2);
		 UART_ClearITPendingBit(MDR_UART2, UART_IT_RX);
	 }
	 
	 if (UART_GetFlagStatus(MDR_UART2, UART_FLAG_TXFE) == SET)
	 {
		 UART_ClearITPendingBit(MDR_UART2, UART_IT_TX);
	 }
 }
 
 
 //System timer
 void SysTick_Handler()
 {
	 sys_time_count--;
 }
